﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

[Flags]
public enum DragConstraints
{
	None = 0,
	X = 1,
	Y = 2,
	XY = 3
}

[RequireComponent( typeof( RectTransform ) )]
public class Draggable : Selectable, IDragHandler, IBeginDragHandler, IEndDragHandler
{
	public DragConstraints freeze;

	public UnityEvent onDrag, onBeginDrag, onEndDrag;

	private RectTransform rectTransform;

	protected override void Awake ()
	{
		rectTransform = GetComponent<RectTransform>();
	}

	void IBeginDragHandler.OnBeginDrag ( PointerEventData eventData )
	{
		onBeginDrag.Invoke();
	}

	void IDragHandler.OnDrag ( PointerEventData eventData )
	{
		Vector2 delta = eventData.delta;

		if ( (freeze & DragConstraints.X) > 0 )
		{
			delta.x = 0f;
		}

		if ( (freeze & DragConstraints.Y) > 0 )
		{
			delta.y = 0f;
		}

		rectTransform.anchoredPosition += delta;

		onDrag.Invoke();
	}

	void IEndDragHandler.OnEndDrag ( PointerEventData eventData )
	{
		onEndDrag.Invoke();
	}
}
