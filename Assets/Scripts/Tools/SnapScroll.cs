﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using Sirenix.OdinInspector;

[HideMonoScript]
[RequireComponent( typeof( RectTransform ) )]
public class SnapScroll : MonoBehaviour, IScrollHandler
{
	[MinValue( 0.0 )]
	public float size = 100f;

	public float animate = 0.1f;

	public RectTransform.Axis axis = RectTransform.Axis.Vertical;

	public bool invert;

	public int limit;

	private RectTransform rect;

	private int index;

	private float time;

	private Vector2 last;
	private Vector2 next;

	void IScrollHandler.OnScroll ( PointerEventData eventData )
	{
		Vector2 delta = eventData.scrollDelta;

		if ( invert )
		{
			delta = -delta;
		}

		index += Mathf.RoundToInt( -delta.y );

		Apply();
	}

	public void Increment ()
	{
		index++;

		Apply();
	}

	public void Decrement ()
	{
		index--;

		Apply();
	}

	private void Apply ()
	{
		index = Mathf.Clamp( index, 0, limit );

		float position = index * size;

		time = Time.time;
		last = rect.anchoredPosition;

		switch ( axis )
		{
		case RectTransform.Axis.Horizontal:
			next = new Vector2( -position, rect.anchoredPosition.y );
			break;

		case RectTransform.Axis.Vertical:
			next = new Vector2( rect.anchoredPosition.x, position );
			break;
		}
		
	}

	private void Awake ()
	{
		rect = GetComponent<RectTransform>();

		last = rect.anchoredPosition;
		next = rect.anchoredPosition;

		time = Time.time - animate;
	}

	private void Update ()
	{
		float t = Mathf.Clamp01( (Time.time - time) / animate );

		rect.anchoredPosition = Vector2.Lerp( rect.anchoredPosition, next, t );
	}
}
