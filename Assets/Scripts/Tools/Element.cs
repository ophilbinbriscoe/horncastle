﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent( typeof( RectTransform ))]
public class Element : MonoBehaviour
{
	public event UnityAction OnReposition;

	private RectTransform rect;

	private void Awake ()
	{
		rect = GetComponent<RectTransform>();
	}

	public void Reposition ( Vector2 position )
	{
		rect.anchoredPosition = position;
	}
}
