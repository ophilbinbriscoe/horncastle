﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent( typeof( Selectable ) )]
public class Unselectable : MonoBehaviour, ISelectHandler
{
	public void OnSelect ( BaseEventData eventData )
	{
		Debug.Log( EventSystem.current.currentSelectedGameObject );
		EventSystem.current.SetSelectedGameObject( null );	
	}
}
