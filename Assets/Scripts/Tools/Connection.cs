﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;
using UnityEngine.UI;

[RequireComponent( typeof( RectTransform ) )]
public class Connection : ScriptableObject
{
	private static Vector2[] empty = new Vector2[2];

	private Element a, b;

	private List<Vector2> points;

	private VectorLine line;

	private void OnEnable ()
	{
		points = new List<Vector2>( empty );

		line = new VectorLine( "", points, 2.0f );
	}

	public void Connect ( Element a, Element b )
	{
		Disconnect();

		this.a = a;
		this.b = b;

		HandleReposition();

		if ( this.a != null )
		{
			this.a.OnReposition += HandleReposition;
		}

		if ( this.b != null )
		{
			this.b.OnReposition += HandleReposition;
		}
	}

	public void Disconnect ()
	{
		if ( a != null )
		{
			a.OnReposition -= HandleReposition;
		}

		if ( b != null )
		{
			b.OnReposition -= HandleReposition;
		}

		a = null;
		b = null;
	}

	private void HandleReposition ()
	{
		line.points2[0] = a.transform.position;
		line.points2[1] = b.transform.position;

		line.Draw();
	}
}
