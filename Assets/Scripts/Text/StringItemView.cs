﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToBoldlyPlay.UI;
using UnityEngine.UI;

public class StringItemView : ListItemView<string>
{
	public Text text;

	public override void Populate ( IObservableListItem<string> item )
	{
		base.Populate( item );

		text.text = item.Value;
	}

	public override void Teardown () { }

	protected override void HandleIndexChanged () { }
}