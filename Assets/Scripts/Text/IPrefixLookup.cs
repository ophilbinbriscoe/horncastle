﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IPrefixLookup
{
	void Lookup ( string prefix, IList<string> results );
}
