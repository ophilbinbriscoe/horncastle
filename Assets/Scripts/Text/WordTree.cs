﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WordTree : WordTreeNode, ICollection<string>, IPrefixLookup
{
	private int count;

	public int Count
	{
		get
		{
			return count;
		}
	}

	public bool IsReadOnly
	{
		get
		{
			return false;
		}
	}

	public WordTree () : base( "", false, new List<WordTreeNode>() ) { }

	public WordTree ( string value ) : base( value, true, new List<WordTreeNode>() ) { }

	private WordTree ( IEnumerable<string> values ) : base( "", false, new List<WordTreeNode>() )
	{
		AddRange( values );
	}

	public void Add ( string value )
	{
		if ( !string.IsNullOrEmpty( value ) )
		{
			if ( Add( value, 0 ) )
			{
				count++;
			}
		}
	}

	public void AddRange ( IEnumerable<string> collection )
	{
		foreach ( var value in collection )
		{
			Add( value );
		}
	}

	public void Clear ()
	{
		isValue = false;
		word = "";
		nodes.Clear();
		count = 0;
	}

	public bool Contains ( string prefix )
	{
		return Contains( prefix, 0 );
	}

	public void CopyTo ( string[] array, int index )
	{
		var list = new List<string>( count );

		Append( list );

		list.CopyTo( array, index );
	}

	public IEnumerator<string> GetEnumerator ()
	{
		var list = new List<string>( count );

		return list.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator ()
	{
		return GetEnumerator();
	}

	public void Lookup ( string prefix, IList<string> results )
	{
		Lookup( prefix, 0, results );
	}

	public bool Remove ( string value )
	{
		if ( Remove( value, 0, null ) )
		{
			count--;

			return true;
		}

		return false;
	}
}
