﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using ToBoldlyPlay.UI;
using ToBoldlyPlay.Pools;

public class SearchBar : MonoBehaviour
{
	[SerializeField]
	[SceneObjectsOnly]
	private InputField input;

	[SerializeField]
	[SceneObjectsOnly]
	private ListView results;

	[SerializeField]
	private ObjectPool pool;

	[SerializeField]
	private ushort max = 10;

	private ListField<string> _results;

	public IPrefixLookup Source { get; set; }

	private void Awake ()
	{
		_results = new ListField<string>( max );

		results.Populate( _results, pool );

		input.onValueChanged.AddListener( ( value ) =>
		{
			if ( Source != null )
			{
				_results.Clear();

				if ( !string.IsNullOrEmpty( value ) )
				{
					Source.Lookup( value, _results );
				}
			}
		} );

		var source = new WordTree();

		source.Add( "dire" );
		source.Add( "diet" );
		source.Add( "dear" );
		source.Add( "dad" );
		source.Add( "daddy" );
		source.Add( "dadio" );
		source.Add( "yule" );
		source.Add( "log" );
		source.Add( "table" );
		source.Add( "lamp" );
		source.Add( "fridge" );
		source.Add( "fridgid" );
		source.Add( "frugal" );
		source.Add( "fear" );
		source.Add( "frog" );
		source.Add( "frogs" );
		source.Add( "fearsome" );
		source.Add( "fearful" );
		source.Add( "fragrant" );
		source.Add( "petulant" );

		source.Remove( "fear" );
		source.Remove( "dadio" );
		source.Remove( "d" );

		Debug.Log( source.Print() );

		Source = source;
	}
}
