﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WordTreeNode : IComparable<WordTreeNode>
{
	protected string word;

	public string Word
	{
		get
		{
			return word;
		}
	}

	protected bool isValue;

	public bool IsValue
	{
		get
		{
			return isValue;
		}
	}

	protected List<WordTreeNode> nodes;

	protected WordTreeNode ( string value, bool isValue, List<WordTreeNode> nodes )
	{
		this.word = value;
		this.isValue = isValue;
		this.nodes = nodes;
	}

	public bool Add ( string value, int index )
	{
		int n = Mathf.Min( this.word.Length, value.Length );

		for ( int i = index; i < n; i++ )
		{
			if ( this.word[i] != value[i] )
			{
				if ( i == index )
				{
					return false;
				}

				var split = new WordTreeNode( this.word, isValue, nodes );

				this.word = this.word.Substring( 0, i );
				isValue = false;

				nodes = new List<WordTreeNode>( 2 );
				nodes.Add( split );

				goto Child;
			}
		}

		index = n;

		if ( index == value.Length )
		{
			return true;	/// added value is same as node's value
		}

		foreach ( var node in nodes )
		{
			if ( node.Add( value, index ) )
			{
				return true;
			}
		}

		Child:

		nodes.Add( new WordTreeNode( value, true, new List<WordTreeNode>() ) );

		nodes.Sort();

		return true;
	}

	public bool Remove ( string value, int index, WordTreeNode ancestor )
	{
		int n = Mathf.Min( this.word.Length, value.Length );

		for ( int i = index; i < n; i++ )
		{
			if ( this.word[i] != value[i] )
			{
				return false;
			}
		}

		if ( this.word.Length == value.Length )
		{
			if ( isValue )
			{
				isValue = false;

				/// Leaf (endpoint) node
				if ( nodes.Count == 0 )
				{
					/// Leaf is root, set to empty string
					if ( ancestor == null )
					{
						this.word = "";
					}

					/// Trim dead leaf
					else
					{
						ancestor.nodes.Remove( this );
					}
				}

				/// Redundant node, collapse to maintain tree health
				else if ( nodes.Count == 1 )
				{
					ancestor.nodes.Remove( this );
					ancestor.nodes.Add( nodes[0] );
					ancestor.nodes.Sort();
				}

				return true;
			}

			return false;
		}
		else
		{
			index = n;

			foreach ( var node in nodes )
			{
				if ( node.Remove( value, index, this ) )
				{
					return true;
				}
			}

			return false;
		}
	}

	public bool Contains ( string prefix, int index )
	{
		int n = Mathf.Min( word.Length, prefix.Length );

		for ( int i = index; i < n; i++ )
		{
			if ( word[i] != prefix[i] )
			{
				return false;
			}
		}

		index = n;

		if ( index == prefix.Length )
		{
			return true;
		}
		else
		{
			foreach ( var node in nodes )
			{
				if ( node.Contains( prefix, index ) )
				{
					return true;
				}
			}

			return false;
		}
	}

	public void Lookup ( string prefix, int index, IList<string> results )
	{
		int n = Mathf.Min( word.Length, prefix.Length );

		for ( int i = index; i < n; i++ )
		{
			if ( word[i] != prefix[i] )
			{
				return;
			}
		}

		index = n;

		if ( index == prefix.Length )
		{
			Append( results );
		}
		else
		{
			foreach ( var node in nodes )
			{
				node.Lookup( prefix, index, results );
			}
		}
	}

	public void Append ( IList<string> results )
	{
		if ( isValue )
		{
			results.Add( word );
		}

		foreach ( var node in nodes )
		{
			node.Append( results );
		}
	}

	public override string ToString ()
	{
		return (string.IsNullOrEmpty( word ) ? "Ø" : word);
	}

	public string Print ()
	{
		string tree = "";

		Print( "", ref tree );

		return tree;
	}

	public void Print ( string indent, ref string tree )
	{
		tree += indent + ToString() + '\n';

		indent += " ";

		foreach ( var node in nodes )
		{
			node.Print( indent, ref tree );
		}
	}

	int IComparable<WordTreeNode>.CompareTo ( WordTreeNode other )
	{
		return word.CompareTo( other.word );
	}
}
