﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TestPrefixes : MonoBehaviour
{
	private WordTree d;

	public string prefix;

	[ReadOnly]
	public List<string> results;

	[Button( "Test" ) ]
	private void Test ()
	{
		if ( results == null )
		{
			results = new List<string>();
		}
		else
		{
			results.Clear();
		}

		d.Lookup( prefix, results );
	}

	private void Awake ()
	{
		d = new WordTree();
	}
}
