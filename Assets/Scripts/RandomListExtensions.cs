﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class RandomExtensions
{
	private static List<int> indices = new List<int>();

	public static T Random<T> ( this IList<T> list )
	{
		if ( list.Count == 0 )
		{
			throw new InvalidOperationException( "Cannot pick a random element from an empty list." );
		}

		return list[UnityEngine.Random.Range( 0, list.Count )];
	}

	public static void Shuffle<T> ( this IList<T> list, Action<T> callback )
	{
		indices.Clear();

		if ( indices.Capacity < list.Count )
		{
			indices.Capacity = list.Count;
		}

		for ( int i = 0; i < list.Count; i++ )
		{
			indices.Add( i );
		}

		int n = list.Count - 2;

		for ( int i = 0; i < n; i++ )
		{
			int k = indices[i];

			int j = UnityEngine.Random.Range( i, list.Count );

			indices[i] = indices[j];
			indices[j] = k;
		}

		for ( int i = 0; i < list.Count; i++ )
		{
			callback( list[indices[i]] );
		}
	}

	public static void ShuffleInto<T> ( this IList<T> list, Queue<T> queue )
	{
		Shuffle( list, v => queue.Enqueue( v ) );
	}

	public static void ShuffleInto<T> ( this IList<T> list, Stack<T> stack )
	{
		Shuffle( list, v => stack.Push( v ) );
	}

	public static void ShuffleInto<T> ( this IList<T> list, IList<T> other )
	{
		Shuffle( list, v => other.Add( v ) );
	}
}
