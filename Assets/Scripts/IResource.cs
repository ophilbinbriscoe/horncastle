﻿using System;
using System.Collections.Generic;

public interface IResource
{
	IEnumerable<string> Tags { get; }

	void AddTag ( string tag );
	void RemoveTag ( string tag );

	Guid ID { get; }
}
