﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Narratives;

public class TestMatching : MonoBehaviour
{
	public Line[] lines;

	public string[] symbols;

	[Button( "Test" )]
	private void Test ()
	{
		var expression = new Pattern();

		foreach ( var symbol in symbols )
		{
			expression.symbols.Add( new Symbol( symbol ) );
		}

		Debug.Log( expression.Matches( lines ) );
	}
}
