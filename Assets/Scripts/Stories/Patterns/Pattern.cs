﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

namespace Narratives
{
	[Serializable]
	public class Pattern
	{
		public List<Symbol> symbols;

		public Pattern ()
		{
			symbols = new List<Symbol>();
		}

		public Pattern ( params Symbol[] symbols )
		{
			this.symbols = new List<Symbol>( symbols );
		}

		public Pattern ( IEnumerable<Symbol> symbols )
		{
			this.symbols = new List<Symbol>( symbols );
		}

		public bool Matches<T> ( IList<T> blocks ) where T : Block
		{
			for ( int symIndex = 0, blkIndex = 0; blkIndex < blocks.Count; blkIndex++ )
			{
				var blkTags = blocks[blkIndex].tags;
				var symTags = symbols[symIndex].tags;

				foreach ( var tag in symTags )
				{
					if ( !blkTags.Contains( tag ) )
					{
						symIndex = 0;

						continue;
					}
				}

				if ( ++symIndex == symbols.Count )
				{
					return true;
				}
			}

			return false;
		}

		public void Compile ()
		{

		}
	}
}
