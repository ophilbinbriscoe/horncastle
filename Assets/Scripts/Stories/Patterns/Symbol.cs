﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narratives
{
	public enum SymbolFlags : byte
	{
		None = 0,
		Not = 1,
		Start = 2
	}

	public struct Symbol
	{
		public Tag[] tags;
		public SymbolFlags flags;
		public byte qty;

		public Symbol ( params Tag[] tags )
		{
			this.tags = tags;
			this.flags = SymbolFlags.None;
			this.qty = 1;
		}

		public Symbol ( SymbolFlags flags, params Tag[] tags )
		{
			this.tags = tags;
			this.flags = flags;
			this.qty = 1;
		}

		public Symbol ( SymbolFlags flags, byte qty, params Tag[] tags )
		{
			this.tags = tags;
			this.flags = flags;
			this.qty = qty;
		}
	}

	public static class SymbolExtensions
	{
		public static SymbolFlags With ( this SymbolFlags operand, SymbolFlags flags )
		{
			return operand | flags;
		}

		public static SymbolFlags Without ( this SymbolFlags operand, SymbolFlags flags )
		{
			return operand & ~flags;
		}

		public static bool HasFlags ( this Symbol symbol, SymbolFlags flags )
		{
			return (symbol.flags & flags) == flags;
		}
	}
}
