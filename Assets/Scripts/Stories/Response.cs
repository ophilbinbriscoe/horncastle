﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Narratives
{
	public class Response
	{
		public Pattern expression;
		public List<Line> lines;
	}
}
