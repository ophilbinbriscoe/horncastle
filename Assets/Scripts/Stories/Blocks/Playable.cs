﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Narratives
{
	[Serializable]
	public abstract class Playable : Block
	{
		public abstract void Play ( Character character );
	}
}
