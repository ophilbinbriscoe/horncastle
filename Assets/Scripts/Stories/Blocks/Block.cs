﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Narratives
{
	[Serializable]
	public class Block
	{
		public List<Tag> tags;
	}
}
