﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Narratives
{
	[Serializable]
	public class Line : Playable
	{
		public string text;

		public override void Play ( Character character )
		{
			character.Say( this );
		}
	}
}
