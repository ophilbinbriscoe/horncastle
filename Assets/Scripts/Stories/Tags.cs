﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narratives
{
	public static class Tags
	{
		public static WordTree tree;

		public static List<string> literals = new List<string>( 512 );
	}

	public struct Tag
	{
		private int index;

		private Tag ( int index )
		{
			this.index = index;
		}

		public static implicit operator Tag ( string tag )
		{
			if ( Tags.tree.Contains( tag ) )
			{
				return new Tag( Tags.literals.IndexOf( tag ) );
			}

			Tags.tree.Add( tag );
			Tags.literals.Add( tag );

			return new Tag( Tags.literals.Count - 1 );
		}

		public static implicit operator string ( Tag tag )
		{
			return Tags.literals[tag.index];
		}

		public override string ToString ()
		{
			return this;
		}

		public override bool Equals ( object obj )
		{
			return obj is Tag && this == (Tag) obj;
		}

		public override int GetHashCode ()
		{
			return index.GetHashCode();
		}

		public static bool operator == ( Tag lhs, Tag rhs )
		{
			return lhs.index == rhs.index;
		}

		public static bool operator != ( Tag lhs, Tag rhs )
		{
			return lhs.index != rhs.index;
		}
	}
}
