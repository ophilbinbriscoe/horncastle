﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu( fileName = "new Eyebrows.asset", menuName = "Body Part/Eyebrows", order = 450 )]
public class Eyebrows : BodyPart
{
	public Sprite sprite;
}
