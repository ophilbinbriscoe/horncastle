﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Narratives
{
	public class Character : MonoBehaviour
	{
		public List<Tag> tags;

		public List<Response> responses;

		public void Respond ( Conversation conversation )
		{
			for ( int i = 0; i < responses.Count; i++ )
			{
				if ( responses[i].expression.Matches( conversation.lines ) )
				{
					responses[i].lines.Random().Play( this );
				}
			}
		}

		public void Say ( Line line )
		{

		}
	}
}
