﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Narratives;

namespace Narratives
{
	public abstract class Item //: IResource
	{
		public abstract float GetUtility ( Character character );
	}
}
