﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManagement : MonoBehaviour
{
	private void Awake ()
	{
		var balance = new Balance( 8, 55 );

		Debug.Log( balance );

		Debug.Log( 10 - balance );

		Debug.Log( balance - 10 );

		Debug.Log( new Balance( 5, 90 ) < new Balance( 6, 0 ) );

		Debug.Log( new Balance( 912391, 12 ) );
	}
}
