﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;

[Serializable]
public struct Balance
{
	[SerializeField]
	private long cents;

	public int Sign
	{
		get
		{
			return cents < 0 ? -1 : +1;
		}
	}

	public long Dollars
	{
		get
		{
			return cents / 100;
		}

		set
		{
			cents = cents % 100 + value * 100;
		}
	}

	public int Cents
	{
		get
		{
			return Mathf.Abs( (int) (cents % 100) );
		}

		set
		{
			cents = value;
		}
	}

	private Balance ( long cents )
	{
		this.cents = cents;
	}

	public Balance ( int dollars, int cents )
	{
		this.cents = dollars * 100 + cents;
	}

	public override bool Equals ( object obj )
	{
		return obj is Balance && this == (Balance) obj;
	}

	public override int GetHashCode ()
	{
		return cents.GetHashCode();
	}

	public override string ToString ()
	{
		return string.Format( "{0:n0}.{1:00}", Dollars, Cents );
	}

	public static implicit operator int ( Balance balance )
	{
		return Convert.ToInt32( balance.cents / 100f );
	}

	public static implicit operator long ( Balance balance )
	{
		return balance.cents / 100;
	}

	public static implicit operator float ( Balance balance )
	{
		return balance.cents / 100f;
	}

	public static implicit operator double ( Balance balance )
	{
		return balance.cents / 100.0;
	}

	public static implicit operator Balance ( int dollars )
	{
		return new Balance( dollars * 100 );
	}

	public static implicit operator Balance ( long dollars )
	{
		return new Balance( dollars * 100 );
	}

	public static implicit operator Balance ( float dollars )
	{
		return new Balance( Convert.ToInt64( dollars * 100f ) );
	}

	public static implicit operator Balance ( double dollars )
	{
		return new Balance( Convert.ToInt64( dollars * 100.0 ) );
	}

	public static Balance operator + ( Balance lhs, Balance rhs )
	{
		return new Balance( lhs.cents + rhs.cents );
	}

	public static Balance operator - ( Balance lhs, Balance rhs )
	{
		return new Balance( lhs.cents - rhs.cents );
	}

	public static Balance operator + ( Balance lhs, int rhs )
	{
		return new Balance( lhs.cents + rhs * 100 );
	}

	public static Balance operator - ( Balance lhs, int rhs )
	{
		return new Balance( lhs.cents - rhs * 100 );
	}

	public static Balance operator + ( int lhs, Balance rhs )
	{
		return new Balance( lhs * 100 + rhs.cents );
	}

	public static Balance operator - ( int lhs, Balance rhs )
	{
		return new Balance( lhs * 100 - rhs.cents );
	}

	public static Balance operator - ( Balance balance )
	{
		return new Balance( -balance.cents );
	}

	public static Balance operator + ( Balance balance )
	{
		return balance;
	}

	public static bool operator == ( Balance lhs, Balance rhs )
	{
		return lhs.cents == rhs.cents;
	}

	public static bool operator != ( Balance lhs, Balance rhs )
	{
		return lhs.cents != rhs.cents;
	}

	public static bool operator < ( Balance lhs, Balance rhs )
	{
		return lhs.cents < rhs.cents;
	}

	public static bool operator > ( Balance lhs, Balance rhs )
	{
		return lhs.cents > rhs.cents;
	}
}