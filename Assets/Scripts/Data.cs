﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class Data
{
	public static bool pretty = true;

	public static void Save ( object data, params string[] path )
	{
		File.WriteAllText( GetCombinedPath( path ), JsonUtility.ToJson( data, pretty ) );
	}

	public static T Load<T> ( params string[] path )
	{
		return JsonUtility.FromJson<T>( File.ReadAllText( GetCombinedPath( path ) ) );
	}

	public static bool Exists ( params string[] path )
	{
		return File.Exists( GetCombinedPath( path ) );
	}

	private static string GetCombinedPath ( params string[] path )
	{
		string combined = Application.dataPath;

		for ( int i = 0; i < path.Length; i++ )
		{
			combined = Path.Combine( combined, path[i] );
		}

		return combined;
	}
}
