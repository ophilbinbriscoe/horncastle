﻿namespace Sirenix.OdinInspector.Demos
{
    using UnityEngine;
    using Sirenix.OdinInspector;

    public class ShowAndHideIfExamples : MonoBehaviour
    {
        public bool IsToggled;

        [Header("Show If")]
        [Indent]
        [ShowIf("IsToggled")]
        public Vector2 VisibleWhenToggled;

        [ShowIf("IsNotToggled")]
        [Indent]
        public Vector3 VisibleWhenNotToggled;

        [ShowIf("IsInEditMode")]
        [Indent]
        [Space]
        public Vector3 VisibleOnlyInEditorMode;

        [Header("Hide If")]
        [Indent]
        [HideIf("IsToggled")]
        public Vector2 HiddenWhenToggled;

        [Indent]
        [HideIf("IsNotToggled")]
        public Vector3 HiddenWhenNotToggled;

        [Indent]
        [Space]
        [HideIf("IsInEditMode")]
        public Vector3 HiddenOnlyInEditorMode;

        private bool IsNotToggled()
        {
            return !this.IsToggled;
        }

        private bool IsInEditMode()
        {
            return !Application.isPlaying;
        }
    }
}