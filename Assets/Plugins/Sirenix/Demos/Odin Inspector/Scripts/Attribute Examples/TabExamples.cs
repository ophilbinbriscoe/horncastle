﻿namespace Sirenix.OdinInspector.Demos
{
    using System;
    using UnityEngine;

    public class TabExamples : MonoBehaviour
    {
        [TabGroup("Tab A")]
        public int One;

        [TabGroup("Tab A")]
        public int Two;

        [TabGroup("Tab A")]
        public int Three;

        [TabGroup("Tab B")]
        public string MyString;

        [TabGroup("Tab B")]
        public float MyFloat;

        [TabGroup("Tab C")]
        [HideLabel]
        public MyTabObject TabC;

        [Serializable]
        public class MyTabObject
        {
            public int A;
            public int B;
            public int C;
        }
    }
}