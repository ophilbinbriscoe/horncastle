﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

[OdinDrawer]
public class BalanceDrawer : OdinValueDrawer<Balance>
{
	protected override void DrawPropertyLayout ( IPropertyValueEntry<Balance> entry, GUIContent label )
	{
		entry.SmartValue = EditorGUILayout.DoubleField( label, entry.SmartValue );
	}

	protected override void DrawPropertyRect ( Rect position, IPropertyValueEntry<Balance> entry, GUIContent label )
	{
		entry.SmartValue = EditorGUI.DoubleField( position, label, entry.SmartValue );
	}

	protected override float GetRectHeight ( IPropertyValueEntry<Balance> entry, GUIContent label )
	{
		return EditorGUIUtility.singleLineHeight;
	}
}
